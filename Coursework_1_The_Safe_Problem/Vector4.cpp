// DO TEMPLATE
#include <iostream>
#include "Vector4.h"

using namespace std;

Vector4::Vector4() : a(0), b(0), c(0), d(0)
{

}

Vector4::Vector4(const int a, const int b, const int c, const int d) : a(a), b(b), c(c), d(d) {
}

Vector4::Vector4(const int x)
{
	a = (x / 1000) % 10;
	b = (x / 100) % 10;
	c = (x / 10) % 10;
	d = x % 10;
}

// overloading the print operator, so I can print 4 numbers easier.
ostream& operator<<(std::ostream& o, const Vector4& v) {
	o << "(" << v.a << "," << v.b << "," << v.c << "," << v.d << ")";
	return o;
}

// The HASHing operator... I might also have to write one for +=
Vector4 Vector4::operator+(const Vector4 & rhs) {

	/*if (rhs.a == 4 && rhs.b == 4) {
		cout << "hi";
	}*/

	int temp[4];
	temp[0] = a + rhs.a;
	temp[1] = b + rhs.b;
	temp[2] = c + rhs.c;
	temp[3] = d + rhs.d;

	if (temp[0] > 9)
		temp[0] %= 10;
	if (temp[0] < 0)
		temp[0] = temp[0] + 10;
	if (temp[1] > 9)
		temp[1] %= 10;
	if (temp[1] < 0)
		temp[1] = temp[1] + 10;
	if (temp[2] > 9)
		temp[2] %= 10;
	if (temp[2] < 0)
		temp[2] = temp[2] + 10;
	if (temp[3] > 9)
		temp[3] %= 10;
	if (temp[3] < 0)
		temp[3] = temp[3] + 10;

	return Vector4(temp[0], temp[1], temp[2], temp[3]);
}

 
//Vector4 Vector4::operator=(const Vector4 & rhs)
//{
//	return Vector4(a = rhs.a, b = rhs.b, c = rhs.c, d = rhs.d);
//}

// returning a reference instead of vector4 will improve the performance because you don't have to create a new object
Vector4& Vector4::operator=(const Vector4 & rhs)
{
	if (this != &rhs) {
		a = rhs.a;
		b = rhs.b;
		c = rhs.c;
		d = rhs.d;
	}

	return *this;
}

Vector4& Vector4::operator=(const int & rhs)
{
	a = (rhs / 1000) % 10;
	b = (rhs / 100) % 10;
	c = (rhs / 10) % 10;
	d = rhs % 10;
	return *this;
}

bool Vector4::operator==(const Vector4 & rhs)
{
	if (a == rhs.a && b == rhs.b && c == rhs.c && d == rhs.d)
		return true;
	else
		return false;
}

Vector4 Vector4::operator-(const Vector4 & rhs)
{
	int temp[4];
	temp[0] = a - rhs.a;
	temp[1] = b - rhs.b;
	temp[2] = c - rhs.c;
	temp[3] = d - rhs.d;

	if (temp[0] > 9)
		temp[0] %= 10;
	if (temp[0] < 0)
		temp[0] = temp[0] + 10;
	if (temp[1] > 9)
		temp[1] %= 10;
	if (temp[1] < 0)
		temp[1] = temp[1] + 10;
	if (temp[2] > 9)
		temp[2] %= 10;
	if (temp[2] < 0)
		temp[2] = temp[2] + 10;
	if (temp[3] > 9)
		temp[3] %= 10;
	if (temp[3] < 0)
		temp[3] = temp[3] + 10;

	return Vector4(temp[0], temp[1], temp[2], temp[3]);

}

int const Vector4::getInteger()
{
	return a*1000 + b*100 + c*10 + d;
}

void Vector4::setA(int a) 
{
	this->a = a;
}

void Vector4::setB(int b)
{
	this->b = b;
}

void Vector4::setC(int c)
{
	this->c = c;
}

void Vector4::setD(int d)
{
	this->d = d;
}

int Vector4::getA() const
{
	return a;
}

int Vector4::getB() const
{
	return b;
}

int Vector4::getC() const
{
	return c;
}

int Vector4::getD() const
{
	return d;
}

// should only be used by the Input_Output
void Vector4::push(int x)
{

	if (!ba) {
		a = x;
		ba = true;
	}
	else if (!bb) {
		b = x;
		bb = true;
	}
	else if (!bc) {
		c = x;
		bc = true;
	}
	else if (!bd) {
		d = x;
		bd = true;
	}
	else {
		// do nothing, because limit is 4
	}
}

// indicating collision, therefore it should inform other classes to generate a new root
bool Vector4::duplicateCN()
{
	if (a == b || a == c || a == d || b == c || b == d || c == d) {

		return true; 

	} else {

		return false;

	}

}
