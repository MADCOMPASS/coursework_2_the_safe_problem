#pragma once
#include "Randomisation.h"
#include "Vector4.h"
#include "New_Hash_Functions.h"

class Lock {
public:
	Lock(New_Hash_Functions* hf);

	bool locking(Vector4 root);
	bool multi_locking(Lock*& lock);  // passing the address of the root lock to another lock to access information to compile their own information.

	bool bonus_multi_locking(Lock *& lock, int position);

	Vector4 const getRoot();
	Vector4 const displayCN();
	Vector4 const getlockDisplay();
	Vector4 const getHN();
	int const getPosition();
	int const getSumEven();
	void setPosition(int x);


private:
	
	Vector4 root;
	Vector4 key; // CN
	Vector4 lockDisplay; // LN
	Vector4 HN; // HN

	Randomisation* random;
	New_Hash_Functions* HF;

	bool status; // open or close
	int sumEven; // to indicate the position of the lock in a multi-lock system. Potentially useful to find a more useful algorithem
	int position;
};