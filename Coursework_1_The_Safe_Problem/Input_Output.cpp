#include <iostream>
#include <fstream>
#include "Input_Output.h"
using namespace std;

//https://stackoverflow.com/questions/8888748/how-to-check-if-given-c-string-or-char-contains-only-digits
bool Input_Output::is_digits(const string &str)
{
return str.find_first_not_of("0123456789") == std::string::npos;
}

void Input_Output::outputKey()
{
	do {

		// so coursework 2 can skip this line
		if (coursework2)
			break;

		cout << "Enter a number (up to 10000) of random roots, otherwise press C for 100 valid solutions" << endl;
		cin >> input;
		if (input == "C") {
			courseworkCriteria = true;
		}
		else if (is_digits(input)) { // checking if the string input contains integer
			loop = stoi(input); // converting string to integer;
		}

	} while (input != "C" && !is_digits(input));

	ofstream myfile;

	try {
		myfile.open("key.txt");
	}
	catch (exception e1) {
		cout << "key file is missing..." << endl;
	}
	//if (!coursework2) {
		cout << "Do you want to include BONUS_MULTI_SAVE ? [Y / N]" << endl;
		cin >> input;
		if (input == "Y")
			bonus = true;
	//}

	for (int i = 0; i < loop; i++) {

		// Returns true if there is collision, false other wise

		if (!coursework2) {
			if (rl->locking(r->getRoot()))
				continue;
		}
		else {
			if (rl->locking(*Root.at(i)))
				continue;
		}

			if (bonus) {
				if (l1->bonus_multi_locking(rl,4))
					continue;
				if (l2->bonus_multi_locking(l1,4))
					continue;
				if (l3->bonus_multi_locking(l2,4))
					continue;
				if (l4->bonus_multi_locking(l3,4))
					continue;
			}
			else {
				if (l1->multi_locking(rl))
					continue;
				if (l2->multi_locking(l1))
					continue;
				if (l3->multi_locking(l2))
					continue;
				if (l4->multi_locking(l3))
					continue;
			}
		
			if (!coursework2) {
				++goodRoots;
				myfile << "NS " << goodRoots << endl;
				myfile << "ROOT " << rl->getRoot() << endl;
				myfile << "UHF " << HF->get_UHF() << endl;
				myfile << "LHF " << HF->get_LHF() << endl;
				myfile << "PHF " << HF->get_PHF() << endl;
			}
			else {
				++goodRoots;
				myfile << "NS " << goodRoots << endl;
				myfile << "ROOT " << *Root.at(i) << endl;
				myfile << "UHF " << *UHF.at(i) << endl;
				myfile << "LHF " << *LHF.at(i) << endl;
				myfile << "PHF " << *PHF.at(i) << endl;
			}

		if (courseworkCriteria) {
			if (goodRoots == 100)
				break;
		}

	}

	myfile.close();

}

void Input_Output::readIn() {

	string line;
	ifstream myfile2;
	myfile2.exceptions(ios::failbit | ios::badbit); // allows the file to throw exception in the internal stream bufer
	bool firstSet = true;

	// expception for reading in the file
	try {
		myfile2.open("readKey.txt",ios::in);
	}
	catch (ios_base::failure err) { // catching the failure exception 
		cout << "readKey file is missing..." << endl;
		system("pause");
	}

	// peek will return EOF if there is nothing to read
	while (myfile2.peek() != EOF) {

		getline(myfile2, line);

		if (line[0] == 'N')
			continue;

		// readin the root number
		if (line[0] == 'R') {
			Vector4* vecTemp = new Vector4();
			for (unsigned int i = 0; i < line.size(); i++) {
				string temp(1, line[i]);
				if (is_digits(temp)) {
					int intTemp = stoi(temp);
					vecTemp->push(intTemp); // forms a vector4
				}

			}
			Root.push_back(vecTemp); // after the vector4 is completed, push it to one of the vector
		}

		// readin the UHF key
		if (line[0] == 'U') {
			Vector4* vecTemp = new Vector4();
			int intTemp;
			for (unsigned int i = 0; i < line.size(); i++) {
				string temp(1, line[i]);
				if (temp == "-") {
					temp += line[i + 1];
					intTemp = stoi(temp);
					vecTemp->push(intTemp);
					i++;
				}
				else if (is_digits(temp)) {
					intTemp = stoi(temp);
					vecTemp->push(intTemp);
				}
			}
			UHF.push_back(vecTemp);
		}

		// readin the LHF key
		if (line[0] == 'L') {
			Vector4* vecTemp = new Vector4();
			int intTemp;
			for (unsigned int i = 0; i < line.size(); i++) {
				string temp(1, line[i]);
				if (temp == "-") {
					temp += line[i + 1];
					intTemp = stoi(temp);
					vecTemp->push(intTemp);
					i++;
				} else if (is_digits(temp)) {
					intTemp = stoi(temp);
					vecTemp->push(intTemp);
				}
			}
			LHF.push_back(vecTemp);
		}

		// readin the PHF key
		if (line[0] == 'P') {
			Vector4* vecTemp = new Vector4();
			int intTemp;
			for (unsigned int i = 0; i < line.size(); i++) {
				string temp(1, line[i]);
				if (temp == "-") {
					temp += line[i + 1];
					intTemp = stoi(temp);
					vecTemp->push(intTemp);
					i++;
				}
				else if (is_digits(temp)) {
					intTemp = stoi(temp);
					vecTemp->push(intTemp);
				}
			}
			PHF.push_back(vecTemp);
		}
	}

	myfile2.close();
}

void Input_Output::multi_safe_file()
{

	ofstream myfile3;
	bool valid = true;

	//auto 

	try {
		 myfile3.open("multiSafe.txt");
	}
	catch (exception e1) {
		cout << "key file is missing..." << endl;
	}

	for (unsigned int i = 0; i < Root.size(); i++) {

			HF->set_UHF(UHF.at(i));
			HF->set_LHF(LHF.at(i));
			HF->set_PHF(PHF.at(i));

		if (rl->locking(*(Root.at(i)))) { // deferencing the pointer in order to get the value
			valid = false;
		}
		if (l1->multi_locking(rl)) {
			valid = false;
		}
		if (l2->multi_locking(l1)) {
			valid = false;
		}
		if (l3->multi_locking(l2)) {
			valid = false;
		} 
		if (l4->multi_locking(l3)) {
			valid = false;
		}
			
			myfile3 << endl;

			myfile3 << "CN" << 0;
			myfile3 << rl->displayCN();
			myfile3 << "LN" << 0;
			myfile3 << rl->getlockDisplay();
			myfile3 << "HN" << 0;
			myfile3 << rl->getHN() << endl;
		
			myfile3 << "CN" << 1;
			myfile3 << l1->displayCN();
			myfile3 << "LN" << 1;
			myfile3 << l1->getlockDisplay();
			myfile3 << "HN" << 1;
			myfile3 << l1->getHN() << endl;

			myfile3 << "CN" << 2;
			myfile3 << l2->displayCN();
			myfile3 << "LN" << 2;
			myfile3 << l2->getlockDisplay();
			myfile3 << "HN" << 2;
			myfile3 << l2->getHN() << endl;

			myfile3 << "CN" << 3;
			myfile3 << l3->displayCN();
			myfile3 << "LN" << 3;
			myfile3 << l3->getlockDisplay();
			myfile3 << "HN" << 3;
			myfile3 << l3->getHN() << endl;

			myfile3 << "CN" << 4;
			myfile3 << l4->displayCN();
			myfile3 << "LN" << 4;
			myfile3 << l4->getlockDisplay();
			myfile3 << "HN" << 4;
			myfile3 << l4->getHN() << endl;

			myfile3 << "NS" << (i + 1);
		

		if (valid)
			myfile3 << " VALID" << endl;
		 else 
			myfile3 << " NOT VALID" << endl;

		valid = true;
	}
	myfile3.close();

}

void Input_Output::locked_safe_file(){

	ofstream myfile4;
	try {
		myfile4.open("lockedSafe.txt");
		
	}
	catch (exception e1) {
		cout << "key file is missing..." << endl;
	}

	/*Randomisation* rando = new Randomisation();
	Lock* rootLock = new Lock(HF);
	Lock* lock1 = new Lock(HF);
	Lock* lock2 = new Lock(HF);
	Lock* lock3 = new Lock(HF);
	Lock* lock4 = new Lock(HF);*/

	for (int i = 0; i < 10000; i++) {

		Vector4* rootTemp = new Vector4();

		*rootTemp = r->getRoot();

		if (rl->locking(*rootTemp))
			continue;
		if (l1->multi_locking(rl))
			continue;
		if (l2->multi_locking(l1))
			continue;
		if (l3->multi_locking(l2))
			continue;
		if (l4->multi_locking(l3))
			continue;

		++goodRoots;
		Root.push_back(rootTemp);

		LN0.push_back(rl->getlockDisplay());
		LN1.push_back(l1->getlockDisplay());
		LN2.push_back(l2->getlockDisplay());
		LN3.push_back(l3->getlockDisplay());
		LN4.push_back(l4->getlockDisplay());
	}

	myfile4 << "NL " << goodRoots << endl;
	for (int i = 0; i < Root.size(); i++) {

		myfile4 << endl;
		myfile4 << "ROOT" << *(Root.at(i)) << endl;
		myfile4 << "LN0" << LN0.at(i) << endl;
		myfile4 << "LN1" << LN1.at(i) << endl;
		myfile4 << "LN2" << LN2.at(i) << endl;
		myfile4 << "LN3" << LN3.at(i) << endl;
		myfile4 << "LN4" << LN4.at(i) << endl;

	}

	myfile4.close();
}

void Input_Output::deriveCN() {

	// booleans to enable so ifstream functions for coursework 1 will also work for courswork2
	coursework2 = true;

	string line;
	ifstream myfile5;
	myfile5.exceptions(ios::failbit | ios::badbit);

	try {
		myfile5.open("lockedSafe.txt",ios::in);
	}
	catch (ios_base::failure err) {
		cout << "lockedSafe file is missing..." << endl;
	}

	while (myfile5.peek() != EOF) {

		getline(myfile5, line);

		if (line.size() == 0) {
			continue;
		}
		// root number
		if (line[2] == 'O') {
			Vector4* vecTemp = new Vector4();
			for (int i = 3; i < line.size(); i++) {
				string strTemp (1, line[i]);
				if (is_digits(strTemp)) {
					int intTemp = stoi(strTemp);
					vecTemp->push(intTemp);
				}
			}
			Root.push_back(vecTemp);
		}

		// LN0
		if (line[2] == '0') {
			Vector4 vecTemp;
			for (int i = 3; i < line.size(); i++) {
				string strTemp(1, line[i]);
				if (is_digits(strTemp)) {
					int intTemp = stoi(strTemp);
					vecTemp.push(intTemp);
				}
			}
			LN0.push_back(vecTemp);
		}

		// LN1
		if (line[2] == '1') {
			Vector4 vecTemp;
			for (int i = 3; i < line.size(); i++) {
				string strTemp(1, line[i]);
				if (is_digits(strTemp)) {
					int intTemp = stoi(strTemp);
					vecTemp.push(intTemp);
				}
			}
			LN1.push_back(vecTemp);
		}

		// LN2
		if (line[2] == '2') {
			Vector4 vecTemp;
			for (int i = 3; i < line.size(); i++) {
				string strTemp(1, line[i]);
				if (is_digits(strTemp)) {
					int intTemp = stoi(strTemp);
					vecTemp.push(intTemp);
				}
			}
			LN2.push_back(vecTemp);
		}

		// LN3
		if (line[2] == '3') {
			Vector4 vecTemp;
			for (int i = 3; i < line.size(); i++) {
				string strTemp(1, line[i]);
				if (is_digits(strTemp)) {
					int intTemp = stoi(strTemp);
					vecTemp.push(intTemp);
				}
			}
			LN3.push_back(vecTemp);
		}

		// LN4
		if (line[2] == '4') {
			Vector4 vecTemp;
			for (int i = 3; i < line.size(); i++) {
				string strTemp(1, line[i]);
				if (is_digits(strTemp)) {
					int intTemp = stoi(strTemp);
					vecTemp.push(intTemp);
				}
			}
			LN4.push_back(vecTemp);
		}

	}

	// codes below are breaking the locks
	Vector4 ULP = LN3[0] - LN2[0];
	Vector4 RTOL = LN0[0] - *Root[0];
	PHF_key = ULP - RTOL;
	Vector4 UL = ULP - PHF_key; // UHF and LHF left that its needed
	bool found = false;

	for (int i = 0; i < 10000; i++) {

		Vector4 LHFtemp = i;
		Vector4 temp;
		int counter = 0;

		UHF_key = UL - LHFtemp; // pretend temp is LHF key, in order to get UFH key

		

		for (int j = 0; j < Root.size(); j++) {
			temp = (*Root.at(j) + UHF_key);
			if (temp.duplicateCN())
				break;
			if (LN0[j] + PHF_key + UHF_key + LHFtemp == LN1[j] && LN1[j] + PHF_key + UHF_key + LHFtemp == LN2[j] && LN2[j] + PHF_key + UHF_key + LHFtemp == LN3[j] && LN3[j] + PHF_key + UHF_key + LHFtemp == LN4[j]) {
				counter++;
				if (counter == Root.size()) {
					LHF_key = LHFtemp;
					found = true;
					break;
				}
			}
		}

		if (found) {
			break;
		}

		
	}

	//Randomisation* rando = new Randomisation();
	//New_Hash_Functions* HF = new New_Hash_Functions();

	for (int i = 0; i < Root.size(); i++) {
		UHF.push_back(&UHF_key);
		LHF.push_back(&LHF_key);
		PHF.push_back(&PHF_key);
	}

	HF->set_UHF(&UHF_key);
	HF->set_LHF(&LHF_key);
	HF->set_PHF(&PHF_key);

	loop = Root.size();

	outputKey();
	multi_safe_file();

	myfile5.close();
}

void Input_Output::multipleLocks()
{
	ofstream myfile;

	try {
		myfile.open("key.txt");
	}
	catch (exception e1) {
		cout << "key file is missing..." << endl;
	}
	
	cout << "Do you want to include BONUS_MULTI_SAVE? [Y/N]" << endl;
	cin >> input;
	if (input == "Y")
		bonus = true;


	int x;
	vector<Lock*> locks;

	cout << "How many locks would you like in a multi-lock?" << endl;

	cin >> x;

	// create the locks in a multi_lock
	for (int i = 0; i < x; i++) {

		Lock* hi = new Lock(HF);

		locks.push_back(hi);

	}
	
	bool print = true;
	// run it for all roots
	for (int j = 0; j < 10000; j++) {
		// if the first lock has dup in cn
		if (locks.at(0)->locking(r->getRoot())) 
			continue;

		// locking the locks
		for (int i = 1; i < locks.size(); i++) {

				// if using bonus requirement
				if (bonus) {

					if (locks[i]->bonus_multi_locking(locks[i - 1],locks.size()-1)) {
						print = false;
						break;
					}

				} else {

					if (locks[i]->multi_locking(locks[i - 1])) {
						print = false;
						break;
					}
				}
		}

		if (print) {
			++goodRoots;
			myfile << "NS " << goodRoots << endl;
			myfile << "ROOT " << locks.at(0)->getRoot() << endl;
			myfile << "UHF " << HF->get_UHF() << endl;
			myfile << "LHF " << HF->get_LHF() << endl;
			myfile << "PHF " << HF->get_PHF() << endl;
		}

		print = true;
	}
	myfile.close();
}

Input_Output::Input_Output(Randomisation * r, New_Hash_Functions * HF, Lock * rl, Lock * l1, Lock * l2, Lock * l3, Lock * l4)
{
	this->r = r;
	this->HF = HF;
	this->rl = rl;
	this->l1 = l1;
	this->l2 = l2;
	this->l3 = l3;
	this->l4 = l4;

	// user input
	do {
		cout << "1, output a 'key' [A]" << endl;
		cout << "2, read in a 'readKey and output a 'multiSafe' [B]" << endl;
		cout << "3, output a 'lockedSafe' [C]" << endl;
		cout << "4, decrypt 'lockedSafe' and output a 'key' and 'multisafe' [D] " << endl;
		cout << "5, change the number locks in a multi_lock [E]" << endl;

		cin >> input;
	
		if (input == "A") {
			outputKey();
		}
		else if (input == "B") {
			readIn();
			multi_safe_file();
		}
		else if (input == "C") {
			locked_safe_file(); // removed the paramter, double check if this works correctly.
		}
		else if (input == "D") {
			deriveCN();
			
		}
		else if (input == "E") {
			multipleLocks();
		}

		

	} while (input != "A" && input != "B" && input != "C" && input != "D" && input != "E" && input != "Y" && input!= "N");

	


	}

