#include "New_Hash_Functions.h"
#include <iostream>

using namespace std;

//New_Hash_Functions::New_Hash_Functions(Vector4 UHF, Vector4 LHF, Vector4 PHF)
//{
//	UHF_key
//}

New_Hash_Functions::New_Hash_Functions(Randomisation * rando)
{
	HashRando = rando;
}

New_Hash_Functions::New_Hash_Functions() // you cant generate a new key for your hash function, if you call the default constructor 
{
}

Vector4 New_Hash_Functions::hash_function(Vector4 x, char hf)
{
	switch (hf) {

	case 'U':
		// converting a 4 digit number to 4 single digit numbers and "HASHING"
		temp = (UHF_key + x);
		return temp;
		break;
	
	case 'L':

		temp = (LHF_key + x);
		return temp;
		break;

	case 'P':

		temp = (PHF_key + x);
		return temp;
		break;

	default:

		cout << "Your second parameter is incorrect, please enter 'U', 'L' or 'P'" << endl;
		return -2;
	}
}

void New_Hash_Functions::generate_new_key()
{
	
	for (int i = 0; i < 4; i++) {
		UHF_key.setA(HashRando->getKey());
		UHF_key.setB(HashRando->getKey());
		UHF_key.setC(HashRando->getKey());
		UHF_key.setD(HashRando->getKey());
		LHF_key.setA(HashRando->getKey());
		LHF_key.setB(HashRando->getKey());
		LHF_key.setC(HashRando->getKey());
		LHF_key.setD(HashRando->getKey());
		PHF_key.setA(HashRando->getKey());
		PHF_key.setB(HashRando->getKey());
		PHF_key.setC(HashRando->getKey());
		PHF_key.setD(HashRando->getKey());
	}
}

Vector4 const New_Hash_Functions::get_UHF()
{
	return UHF_key;
}

Vector4 const New_Hash_Functions::get_LHF()
{
	return LHF_key;
}

Vector4 const New_Hash_Functions::get_PHF()
{
	return PHF_key;
}

void New_Hash_Functions::set_UHF(Vector4* x)
{
	UHF_key = *x;
}

void New_Hash_Functions::set_LHF(Vector4* x)
{
	LHF_key = *x;
}

void New_Hash_Functions::set_PHF(Vector4* x)
{
	PHF_key = *x;
}

