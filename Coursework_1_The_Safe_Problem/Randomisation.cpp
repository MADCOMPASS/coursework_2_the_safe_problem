#include "Randomisation.h"
#include <iostream>
//#include "Vector4.h"

using namespace std;

Randomisation::Randomisation()
{

}

Vector4 Randomisation::getRoot()
{
	
	if (!initialised) {
		
		// multi-threading by passing a lambda expression. The benefits are that you can have encapsulated code passed into another function to promote modules code
		auto f = [this]() { // this allows you to do things to the current class
			vector<Vector4> rootsNum;
			for (int i = 0; i < maxRootIndex + 1; i++) {
				roots.push_back(i);
			}
		};

		thread thread(f);

		thread.join(); // wait for the thread to finish

		initialised = true;
	}

	if (roots.size() != 0) {
		Vector4& tempNum = roots[rand() % roots.size()];
		Vector4 num = tempNum;
		swap(tempNum, roots[roots.size() - 1]);
		roots.pop_back();
		return num;

	} else {
		cout << "you have exhausted all 10000 root number" << endl;
		system("pause");
	}

}

int Randomisation::getKey()
{
	return rand() % (20 + 0) - 10;
}
