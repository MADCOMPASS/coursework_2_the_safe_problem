#pragma once
#include <vector>
#include "Randomisation.h"
#include "Vector4.h"

class New_Hash_Functions {
public:
	// also not using pointers because the things im passing in is not a object pointer either
	//New_Hash_Functions(Vector4 UHF, Vector4 LHF, Vector4 PHF); // used for reading in file, so i can use it to construct a speific hash function that will return what I want, which will be used by the lock to check if root is valid.
	New_Hash_Functions(Randomisation* rando);
	New_Hash_Functions();

	// general Hash function that will hash a number depending on the char. Remember that CN must not have repeating numbers
	Vector4 hash_function(Vector4 x, char hf); // consider returning a boolean
	void generate_new_key(); // not needed until coursework 2

	// to ensure you dont modify the object that you are returning, hence the reason for const 
	Vector4 const get_UHF();
	Vector4 const get_LHF();
	Vector4 const get_PHF();

	// setters for the hash function keys
	void set_UHF(Vector4* x);
	void set_LHF(Vector4* x);
	void set_PHF(Vector4* x);

private:
	Randomisation* HashRando;
	Vector4 temp;// for returning and storing the return value, different to UFH_key which is the hash key			   // hash keys
	Vector4 UHF_key;
	Vector4 LHF_key;
	Vector4 PHF_key;
};