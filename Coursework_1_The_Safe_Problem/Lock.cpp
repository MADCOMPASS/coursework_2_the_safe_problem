#include "Lock.h"
#include "Randomisation.h"
#include <iostream>

using namespace std;

Lock::Lock(New_Hash_Functions* hf){

	HF = hf;
}

bool Lock::multi_locking(Lock*& lock)
{
	key = HF->hash_function(lock->HN, 'U');

	if (key.duplicateCN()) {
		//key = 0;
		lockDisplay = HF->hash_function(key, 'L');
		HN = HF->hash_function(lockDisplay, 'P');
		return true; // indicating collision
	} else {
		lockDisplay = HF->hash_function(key, 'L'); 
		HN = HF->hash_function(lockDisplay, 'P');
		return false;
	}
	return false;
}

bool Lock::bonus_multi_locking(Lock*& lock, int pos)
{
	key = HF->hash_function(lock->HN, 'U');
	sumEven = 0;
	position = lock->getPosition() + 1; // getting the position of the previous lock
	sumEven = lock->getSumEven() + key.getA() + key.getB() + key.getC() + key.getD(); // adding the total sum of numbers from the previous lock

	if (key.duplicateCN()) {
		//sumEven = 0;
		lockDisplay = HF->hash_function(key, 'L');
		HN = HF->hash_function(lockDisplay, 'P');
		return true; // indicating collision
	}
	else {

		int lockLeft = lock->displayCN().getA() + lock->displayCN().getB() + lock->displayCN().getC() + lock->displayCN().getD();
		int currentLock = key.getA() + key.getB() + key.getC() + key.getD();

		// if leftlock is bigger than the currentlock
		if (lockLeft >= currentLock){
			//sumEven = 0;
			return true; // indicating there is a problem
		}
		// if the sum is even
		if (sumEven % 2 != 0 && position == pos) {
			//sumEven = 0;
			return true;
		}

		lockDisplay = HF->hash_function(key, 'L');
		HN = HF->hash_function(lockDisplay, 'P');
		return false;
	}
	return false;
}

Vector4 const Lock::getRoot()
{
	return root;
}

Vector4 const Lock::displayCN()
{
	return key;
}

Vector4 const Lock::getlockDisplay()
{
	return lockDisplay;
}

Vector4 const Lock::getHN()
{
	return HN;
}

int const Lock::getPosition()
{
	return position;
}

int const Lock::getSumEven()
{
	return sumEven;
}

void Lock::setPosition(int x)
{
	position = x;;
}

bool Lock::locking(Vector4 root) 
{
	this->root = root;
	key = HF->hash_function(root, 'U');

	// resetting variables when using looking for the root lock
	position = 0;
	sumEven = 0;

	sumEven = key.getA() + key.getB() + key.getC() + key.getD();

	if (key.duplicateCN()) { // is CN duplicated?
		//key = 0;
		lockDisplay = HF->hash_function(key, 'L');
		HN = HF->hash_function(lockDisplay, 'P');
		return true;
	} else { // if it is then do the rest of the hash functions that does not require the duplicate checks
		lockDisplay = HF->hash_function(key, 'L');
		HN = HF->hash_function(lockDisplay, 'P');
		return false;
	}
}



