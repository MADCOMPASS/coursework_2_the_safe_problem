#pragma once
// To repesent the number system on a LOCK and HASH KEY. 
// I could probably come up with a better name for this class............
#include <iostream>


class Vector4 {
public:

	Vector4();
	Vector4(const int a, const int b, const int c, const int d);
	Vector4(const int x);

	// overloading the print operator, so I can print 4 numbers easier
	friend std::ostream& operator<<(std::ostream& o, const Vector4& v); // I need to research the stuff here

	// The HASHing function operator that includes changing a double digit to a single digit... I might also have to write one for +=
	Vector4 operator+(const Vector4& rhs);

	// Vector4 to Vector4 assignment
	Vector4& operator=(const Vector4& rhs);

	// Vector4 to integer assignment
	Vector4& operator=(const int& rhs);

	bool operator==(const Vector4& rhs);

	Vector4 operator-(const Vector4& rhs);
	

	// returns 4 keys are a single integer
	int const getInteger();

	// setters
	void setA(int a);
	void setB(int b);
	void setC(int c);
	void setD(int d);

	// getters
	int getA() const;
	int getB() const;
	int getC() const;
	int getD() const;

	// pushing a number on to the vector, similar to a stack.
	void push(int x);

	// checks if there are duplicate numbers 
	bool duplicateCN();

private:
	int a, b, c, d;
	bool ba = false;
	bool bb = false;
	bool bc = false;
	bool bd = false;
};

