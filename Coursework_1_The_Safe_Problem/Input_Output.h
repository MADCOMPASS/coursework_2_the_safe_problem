#pragma once
#include "string"
#include "New_Hash_Functions.h"
#include "Lock.h"
#include "Randomisation.h"
#include "Vector4.h"
#include <thread>
#include <vector> 

class Input_Output {

public:
	Input_Output(Randomisation* r, New_Hash_Functions* HF, Lock* rl, Lock* l1, Lock* l2, Lock* l3, Lock* l4);
	bool is_digits(const std::string &str);
	void outputKey();
	void readIn();
	void multi_safe_file();
	void locked_safe_file();
	void deriveCN();
	void multipleLocks();

private:

	// for multisave
	std::vector<Vector4 *> UHF;
	std::vector<Vector4 *> LHF;
	std::vector<Vector4 *> PHF;
	std::vector<Vector4 *> Root;

	// for locked save and derivedCN
	std::vector<Vector4> LN0;
	std::vector<Vector4> LN1;
	std::vector<Vector4> LN2;
	std::vector<Vector4> LN3;
	std::vector<Vector4> LN4;

	Randomisation* r;
	New_Hash_Functions* HF;
	Lock* rl;
	Lock* l1;
	Lock* l2;
	Lock* l3;
	Lock* l4;

	Vector4 UHF_key;
	Vector4 LHF_key;
	Vector4 PHF_key;


	int goodRoots = 0;
	int loop;
	std::string input; // allowing the user to have multiple inputs
	bool courseworkCriteria = false;
	bool bonus = false;
	bool coursework2 = false; // to allow the same function to work for coursework 1 and 2. Using a boolean to change the pathing, but keeping the logic the same.
};