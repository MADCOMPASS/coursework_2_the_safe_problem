#pragma once
#include <vector>
#include "Vector4.h"
#include <thread>

class Randomisation {
public:
	Randomisation();
	Vector4 getRoot();
	int getKey(); 
private:
	int maxRootIndex = 9999;
	bool initialised = false;
	std::vector<Vector4> roots;
};