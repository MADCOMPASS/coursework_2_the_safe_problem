#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
#include <stdlib.h>
#include <chrono>
#include "string"
#include "New_Hash_Functions.h"
#include "Lock.h"
#include "Randomisation.h"
#include "Input_Output.h"

using namespace std;
using namespace std::chrono;
int main() {

	/*unsigned int n = std::thread::hardware_concurrency();
	std::cout << n << " concurrent threads are supported.\n";*/

	// to ensure the same order of random root numbers
	//srand(0);

	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	
	// object orientation 
	Randomisation* set1 = new Randomisation(); // I should only ever need one randomisation object
	New_Hash_Functions* HF = new New_Hash_Functions(set1); // I should only need one Hash Function object, as I can use the same object to generate a new HF
	HF->generate_new_key();

	// manually setting keys so it generate results
	Vector4* UHF = new Vector4(4, 9, 9, 9);
	Vector4* LHF = new Vector4(0, 0, 0, 0);
	Vector4* PHF = new Vector4(0, 0, 0, 0);
	HF->set_UHF(UHF);
	HF->set_LHF(LHF);
	HF->set_PHF(PHF);

	Lock* rootLock = new Lock(HF);
	Lock* Lock1 = new Lock(HF);
	Lock* Lock2 = new Lock(HF);
	Lock* Lock3 = new Lock(HF);
	Lock* Lock4 = new Lock(HF); 

	Input_Output* IO = new Input_Output(set1, HF, rootLock, Lock1, Lock2, Lock3, Lock4);
	
	high_resolution_clock::time_point t2 = high_resolution_clock::now();

	auto duration = duration_cast<milliseconds>(t2 - t1).count();
	cout << "Milliseconds: " << duration << endl;

	system("pause");
}